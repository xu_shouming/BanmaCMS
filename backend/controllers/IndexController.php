<?php
/**
 * Created by MichealZ.
 * Description: 首页
 * Date: 2015/8/19
 */
namespace backend\controllers;
use Yii;
use yii\web\Controller;
    class IndexController extends Controller
    {
        /**
         * Created by MichealZ.
         * Description: 首页
         * Date: 2015/8/19
         */
        public function actionIndex()
        {
            return $this->render('index');
        }
    }
