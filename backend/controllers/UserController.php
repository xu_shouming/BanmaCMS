<?php
/**
 * Created by MichealZ.
 * Description: 用户登录/注销/添加/修改/删除
 * Date: 2015/8/19
 */
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use backend\models\users;
    class UserController extends Controller
    {
        /*随机字符串*/
        protected $randNum = '23456789abcdefghjkmnopqrstuvwxyz';

        /**
         * Created by MichealZ.
         * Description: 生成验证码
         * Date: 2015/8/25
         * param: none
         * return: bool
         */
        public function actionVcode()
        {
            ob_clean(); //清空图片缓冲区
            $im = imagecreatetruecolor(65, 22);
            $backgroundcolor = imagecolorallocate($im, 220, 220, 220); //图片背景色
            imagefill($im, 0, 0, $backgroundcolor);
            $strcolor = imagecolorallocate($im, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); //图片你上画字符
            $str = substr(str_shuffle($this->randNum), 0, 4);
            imagestring($im, 15, 16, 5, $str, $strcolor);
            $linecolor1 = imagecolorallocate($im, mt_rand(90, 150), mt_rand(90, 150), mt_rand(90, 150)); //画干扰线
            $linecolor2 = imagecolorallocate($im, mt_rand(90, 150), mt_rand(90, 150), mt_rand(90, 150));
            $linecolor3 = imagecolorallocate($im, mt_rand(90, 150), mt_rand(90, 150), mt_rand(90, 150));
            imageline($im, 0, mt_rand(0, 22), 70, mt_rand(0, 22), $linecolor1);
            imageline($im, 0, mt_rand(0, 22), 70, mt_rand(0, 22), $linecolor2);
            imageline($im, 0, mt_rand(0, 22), 70, mt_rand(0, 22), $linecolor3);
            header('content-type: image/png');
            imagepng($im);
            imagedestroy($im);
            $this->startSession();
            $this->session->set('vcode', $str);
            return true;
        }

        /**
         * Created by MichealZ.
         * Description: 用户登录
         * Date: 2015/8/19
         * param: none
         * return: view page
         */
        public function actionLogin()
        {
            $this->startSession();
            if(isset($_SESSION['userid']))
                $this->redirect(['index/index']);
            return $this->render('login');
        }

        /**
         * Created by MichealZ.
         * Description: 用户登录校验
         * Date: 2015/8/25
         * param: none
         * return: Json
         */
        public function actionLoginact()
        {
            $this->startSession(); //开启session
            $email = ['email' => Yii::$app->request->post('email')];
            if(!$data = Users::loginValidate($email)) //如果数据库中不存在此用户
                return $this->returnJson(['status' => 'fail', 'msg' => '此邮箱未注册！']);
            else
            {
                $password = $data->password;
                if(Yii::$app->getSecurity()->validatePassword(Yii::$app->request->post('password'), $password)) //验证成功
                {
                    if(Yii::$app->request->post('remember') == 1) //设置记录用户名到cookie 时间为1个月
                        $this->setCookie(['name' => 'remember', 'value' => 'ok'], ['name' => 'email', 'value' => $email['email']] );
                    else
                    {
                        $cookie = Yii::$app->request->cookies; //取消记录用户名，销毁cookie
                        if($cookie->has('remember'))
                            $this->setCookie([], [], false);
                    }
                    $this->session->set('userid', $data->id);
                    $this->session->set('username', $data->username);
                    $this->session->set('email', $data->email);
                    return $this->returnJson(['status' => 'success', 'msg' => '登录成功！']);
                }
                else
                    return $this->returnJson(['status' => 'fail', 'msg' => '密码错误！']);
            }
        }

        /**
         * Created by MichealZ.
         * Description: 用户注销
         * Date: 2015/8/19
         * param: none
         * return: Json
         */
        public function actionLogout()
        {
            $this->startSession();
            unset($_SESSION['userid'], $_SESSION['username']);//销毁session
            $this->closeSession();
            return $this->returnJson(['status' => 'success', 'msg' => '退出成功']);
        }

        /**
         * Created by MichealZ.
         * Description: 用户注册
         * Date: 2015/8/19
         * param: none
         * return: view page
         */
        public function actionRegister()
        {
            return $this->render('register');
        }

        /**
         * Created by MichealZ.
         * Description: 用户注册提交
         * Date: 2015/8/25
         * param: none
         * return: Json
         */
        public function actionRegisteract()
        {
            $this->startSession();
            if(Yii::$app->request->post('vcode') != $_SESSION['vcode']) //验证码错误
                return $this->returnJson(['status' => 'fail', 'msg' => '请点击验证码图片刷新，重新输入！']);
            if(Users::userVemail(Yii::$app->request->post('email'))) //邮箱是否已被注册
                return $this->returnJson(['status' => 'fail', 'msg' => '邮箱已被注册，请更换其他邮箱！']);
            $hash = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('password')); //加密算法
            $registerData = [
                'username' => Yii::$app->request->post('username'),
                'password' => $hash,
                'email' => Yii::$app->request->post('email'),
                'created_at' => $_SERVER['REQUEST_TIME'],
                'role_id' => '3' //默认注册为客服角色
            ];
            if($uid = Users::userReg($registerData)) //注册成功，设置session
            {
                $this->session->set('userid', $uid);
                $this->session->set('username', $registerData['username']);
                return $this->returnJson(['status' => 'success', 'msg' => '注册成功！']);
            }
            else
                return $this->returnJson(['status' => 'fail', 'msg' => '注册失败，请重试！']);
        }

        /**
         * Created by MichealZ.
         * Description: 重置密码账户填写页
         * Date: 2015/8/26
         */
        public function actionEnteracc()
        {
            return $this->render('enteracc');
        }

        /**
         * Created by MichealZ.
         * Description: 重置密码 填写接受邮件邮箱验证
         * Date: 2015/8/26
         * return: Json
         */
        public function actionValidemail()
        {
            $email = ['email' => Yii::$app->request->post('email')];
            if(!Users::loginValidate($email))
                return $this->returnJson(['status' => 'fail', 'msg' => '此邮箱未注册，请重试!']);
            return $this->returnJson(['status' => 'success', 'msg' => '邮箱存在!']);
        }

        /**
         * Created by MichealZ.
         * Description: 邮件发送
         * Date: 2015/8/26
         * return: Json
         */
        public function actionSendmail()
        {
            $email = Yii::$app->request->post('email');
            $secret = md5($email . '+' . self::MD5PASS); //拼接md5串
            $secret = base64_encode($email . '*.*' . $secret);
            $secret = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'] . '?r=user/resetpasswd&secret=' . $secret;
            $mail= Yii::$app->mailer->compose();
            $mail->setTo($email);
            $mail->setSubject('修改密码');
            $mail->setHtmlBody('<br><h2>亲爱的用户您好!</h2><br>请点击下面链接修改您的密码!<br><br>' . $secret);    //发布可以带html标签的文本
            if($mail->send())
                return $this->returnJson(['status' => 'success', 'msg' => '发送成功!']);
            else
                return $this->returnJson(['status' => 'fail', 'msg' => '发送失败!']);
        }

        /**
         * Created by MichealZ.
         * Description: 邮件发送成功or失败 提示页
         * Date: 2015/8/26
         * return: view page AND array
         */
        public function actionNotice()
        {
            $notice['img'] = 'success.png';
            $notice['content'] = '邮件发送成功，请至您的邮箱查收!';
            if(Yii::$app->request->get('error'))
            {
                $notice['img'] = 'error.png';
                $notice['content'] = '邮件发送失败，请联系管理员!';
            }
            else if(Yii::$app->request->get('resetsuccess'))
                $notice['content'] = '密码修改成功!';
            return $this->render('notice', ['msg' => $notice]);
        }

        /**
         * Created by MichealZ.
         * Description: 密码修改页
         * Date: 2015/8/26
         * return: view page AND Array
         */
        public function actionResetpasswd()
        {
            $secret = explode('*.*', base64_decode(Yii::$app->request->get('secret')));
            $msg['img'] = 'error.png';
            $msg['content'] = '请确定您链接与邮箱内发送的一致!';
            if(!Users::userVemail($secret[0])) //邮箱验证失败
                return $this->render('notice', ['msg' => $msg]);
            if(md5($secret[0] . '+' . self::MD5PASS) !== $secret[1]) //邮箱 与 常量验证
                return $this->render('notice', ['msg' => $msg]);
            return $this->render('resetpasswd', ['email' => $secret[0]]);
        }

        /**
         * Created by MichealZ.
         * Description: 密码修改行为
         * Date: 2015/8/27
         * return: Json
         */
        public function actionResetpasswdact()
        {
            $hash = Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('password')); //加密
            $resetPasswdData = [
                'email' => Yii::$app->request->post('email'),
                'password' => $hash,
                'updated_at' => $_SERVER['REQUEST_TIME']
            ];
            if(Users::resetPasswd($resetPasswdData))
                return $this->returnJson(['status' => 'success', 'msg' => '修改成功!']);
            else
                return $this->returnJson(['status' => 'fail', 'msg' => '修改失败!']);
        }

        /**
         * Created by MichealZ.
         * Description: 用户列表
         * Date: 2015/8/19
         * param: none
         * return: view page
         */
        public function actionUlist()
        {
            return $this->render('ulist');
        }

        /**
         * Created by MichealZ.
         * Description: 修改用户权限
         * Date: 2015/8/19
         */
        public function actionUmodifypriv()
        {

        }

        /**
         * Created by MichealZ.
         * Description: 删除用户
         * Date: 2015/8/19
         */
        public function actionUdelete()
        {

        }

        /**
         * Created by MichealZ.
         * Description: 城市管理员列表
         * Date: 2015/8/19
         */
        public function actionCitymanager()
        {
            return $this->render('citymanager');
        }

        /**
         * Created by MichealZ.
         * Description: 城市管理员添加页
         * Date: 2015/8/19
         */
        public function actionCitymanageradd()
        {
            return $this->render('citymanageradd');
        }


    }