<?php
/**
 * Created by MichealZ.
 * Description: 官网表单管理
 * Date: 2015/8/20ss
 */
namespace backend\controllers;
use Yii;
use yii\web\Controller;
    class FormController extends Controller
    {
        /**
         * Created by MichealZ.
         * Description: 招商表单
         * Date: 2015/8/20
         */
        public function actionBusiness()
        {
            return $this->render('business');
        }
        /**
         * Created by MichealZ.
         * Description: 货主表单
         * Date: 2015/8/20
         */
        public function actionBuyer()
        {
            return $this->render('buyer');
        }
        /**
         * Created by MichealZ.
         * Description: 司机表单
         * Date: 2015/8/20
         */
        public function actionDriver()
        {
            return $this->render('driver');
        }
    }
