<?php
/**
 * Created by MichealZ.
 * Description: 司机管理/添加 修改 重置密码 拒绝审核
 * Date: 2015/8/19
 */
namespace backend\controllers;
use Yii;
use yii\web\Controller;
    class DriverController extends Controller
    {
        /**
         * Created by MichealZ.
         * Description: 司机列表
         * Date: 2015/8/19
         */
        public function actionDlist()
        {
            $arr = ['id,name,'];
            return $this->render('dlist');
        }
        /**
         * Created by MichealZ.
         * Description: 司机添加 编辑
         * Date: 2015/8/19
         */A
        public function actionOperation()
        {
            if(Yii::$app->request->get('act') == 'add')
                return $this->render('add'); //新增
            else
                return $this->render('edit'); //编辑
        }
        /**
         * Created by MichealZ.
         * Description: 重置司机密码
         * Date: 2015/8/19
         */
        public function actionResetpass()
        {

        }
        /**
         * Created by MichealZ.
         * Description: 拒绝审核
         * Date: 2015/8/19
         */
        public function actionDeny()
        {

        }
        /**
         * Created by MichealZ.
         * Description: 司机数据统计
         * Date: 2015/8/20
         */
        public function actionDriverscnt()
        {
            if(Yii::$app->request->get('online') == 1)
                return $this->render('driverscnt');
            else
                return $this->render('driversreg');
        }
    }
