<?php
/**
 * Created by MichealZ.
 * Description: 招商列表模版
 * Date: 2015/8/20
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-3-3 uk-container-center">
        <div class="uk-panel">
            <table class="uk-table uk-table-hover uk-table-striped">
                <caption>这些人已填写招商表单</caption>
                <thead>
                <tr>
                    <th>编号</th>
                    <th>姓名</th>
                    <th>电话</th>
                    <th>电子邮件</th>
                    <th>省份</th>
                    <th>城市</th>
                    <th>备注信息</th>
                    <th>填写时间</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>111</td>
                    <td>德玛西亚</td>
                    <td>135555555555</td>
                    <td>124@sina.com</td>
                    <td>california</td>
                    <td>salt lake</td>
                    <td>啦啦啦德玛西亚</td>
                    <td>2015-04-05</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>