<?php
/**
 * Created by MichealZ.
 * User: MichealZ
 * Date: 2015/8/19
 */
@session_start();
?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title>斑马快跑CMS</title>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="<?= CSS_URL ?>uikit.almost-flat.min.css" rel='stylesheet' type='text/css' />
    <link href="<?= CSS_URL ?>app.css" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_URL ?>form-advanced.almost-flat.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_URL ?>form-select.almost-flat.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_URL ?>slidenav.almost-flat.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_URL ?>dotnav.almost-flat.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_URL ?>search.almost-flat.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= CSS_URL ?>autocomplete.almost-flat.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= JS_URL ?>jquery.2.1.4.min.js"></script>
</head>
<body>
<?php $this->beginBody() ?>
<nav class="uk-navbar uk-navbar-attached">
    <a href="#api-offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas>
        <i class="uk-icon-bars"></i>
    </a>
    <div class="uk-container uk-container-center">
        <!--logo图片-->
        <ul class="uk-navbar-nav">
            <li>
                <a style="display: inline-block;" class="navbar-brand" href="index.php?r=index">
                    <img style="height: 100%;" src="http://121.42.24.160/uploads/1432763709871180445.png">
                </a>
            </li>
        </ul>

        <div class="uk-navbar-flip">
            <ul class="uk-navbar-nav uk-hidden-small">
                <?php
                    if(!isset($_SESSION['userid']))
                    {
                        echo '<li><a href="index.php?r=user/login">登录</a></li><li><a href="index.php?r=user/register">注册</a></li>';
                        echo '<input type="hidden" id="loginStatus" value="unlogin">';
                    }
                ?>
                <!--业务管理-->
                <li class="uk-parent" data-uk-dropdown="{mode:'click'}">
                    <a href="">
                        业务管理
                        <i class="uk-icon-caret-down"><img src="<?= IMG_URL ?>uk-icon-caret-down.png"></i>
                    </a>
                    <div class="uk-dropdown uk-dropdown-navbar" role="menu">
                        <ul class="uk-nav uk-nav-navbar">
                            <li class="uk-nav-header">
                                司机管理
                            </li>
                            <li>
                                <a href="index.php?r=driver/dlist">
                                    司机信息详情
                                </a>
                            </li>
                            <li class="uk-nav-divider"></li>
                        </ul>
                        <ul class="uk-nav uk-nav-navbar">
                            <li class="uk-nav-header">
                                货主管理
                            </li>
                            <li class="uk-nav-divider"></li>
                        </ul>
                    </div>
                </li>
                <!--用户管理-->
                <li class="uk-parent" data-uk-dropdown="{mode:'click'}">
                    <a href="">
                        用户管理
                        <i class="uk-icon-caret-down"><img src="<?= IMG_URL ?>uk-icon-caret-down.png"></i>
                    </a>
                    <div class="uk-dropdown uk-dropdown-navbar" role="menu">
                        <ul class="uk-nav uk-nav-navbar">
                            <li class="uk-nav-header">
                                超级管理员
                            </li>
                            <li>
                                <a href="index.php?r=user/citymanager">
                                    创建城市管理员账户
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    设置车辆类型及价格
                                </a>
                            </li>
                            <li class="uk-nav-divider"></li>
                            <li class="uk-nav-header">
                                城市管理员
                            </li>
                            <li>
                                <a href="index.php?r=user/ulist">
                                    创建客服账户
                                </a>
                            </li>
                            <li class="uk-nav-divider"></li>
                        </ul>
                    </div>
                </li>
                <li class="uk-parent" data-uk-dropdown="{mode:'click'}">
                    <a href="">
                        统计数据
                        <i class="uk-icon-caret-down"><img src="<?= IMG_URL ?>uk-icon-caret-down.png"></i>
                    </a>
                    <div class="uk-dropdown uk-dropdown-navbar" role="menu">
                        <ul class="uk-nav uk-nav-navbar">
                            <li class="uk-nav-header">
                                当前司机统计
                            </li>
                            <li>
                                <a href="index.php?r=driver/driverscnt&online=1">
                                    在线司机统计
                                </a>
                            </li>
                            <li>
                                <a href="index.php?r=driver/driverscnt&register=1">
                                    注册司机统计
                                </a>
                            </li>
                            <li class="uk-nav-divider"></li>
                        </ul>
                    </div>
                </li>
                <li class="uk-parent" data-uk-dropdown="{mode:'click'}">
                    <a href="">
                        官网表单管理
                        <i class="uk-icon-caret-down"><img src="<?= IMG_URL ?>uk-icon-caret-down.png"></i>
                    </a>
                    <div class="uk-dropdown uk-dropdown-navbar" role="menu">
                        <ul class="uk-nav uk-nav-navbar">
                            <li class="uk-nav-header">
                                表单详情
                            </li>
                            <li>
                                <a href="index.php?r=form/business">
                                    招商表单
                                </a>
                            </li>
                            <li>
                                <a href="index.php?r=form/buyer">
                                    货主表单
                                </a>
                            </li>
                            <li>
                                <a href="index.php?r=form/driver">
                                    司机表单
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <?php
                    if(isset($_SESSION['userid']))
                    {
                        echo '<li class="uk-parent" data-uk-dropdown={mode:"click"}>
                                <a href="">
                                    <i class="uk-icon-user uk-icon-small"><img src="' . IMG_URL . 'uk-icon-user-b.png"/></i>
                                    ' . $_SESSION['username'] . '
                                    <i class="uk-icon-caret-down"><img src="' . IMG_URL . 'uk-icon-caret-down.png"/></i>
                                </a>
                                <div class="uk-dropdown uk-dropdown-navbar" role="menu">
                                    <ul class="uk-nav uk-nav-navbar"">
                                        <li><a id="logout" href="javascript:void(0)">登出</a></li>
                                    </ul>
                                </div>
                             </li>';
                    }
                ?>

            </ul>
        </div>
    </div>
</nav>

<?= $content ?>

<?php $this->endBody() ?>
<script type="text/javascript" src="<?= JS_URL ?>uikit.min.js"></script>
<script type="text/javascript" src="<?= JS_URL ?>form-select.min.js"></script>
<script type="text/javascript" src="<?= JS_URL ?>form-password.min.js"></script>
<script type="text/javascript" src="<?= JS_URL ?>dropdown.min.js"></script>
<script type="text/javascript" src="<?= JS_URL ?>lightbox.min.js"></script>
<script type="text/javascript" src="<?= JS_URL ?>search.min.js"></script>
<script type="text/javascript" src="<?= JS_URL ?>autocomplete.min.js"></script>
</body>
</html>
<?php
$script = <<< JS
    /*未登录的情况下 影藏功能菜单*/
    $(function(){
        if($('#loginStatus').val() == 'unlogin'){
            $('.uk-parent').css('display', 'none');
        }
    });
    /*退出登录*/
    $('#logout').click(function(){
        $.get('index.php?r=user/logout', function(data){
            var json = JSON.parse(data);
            if(json.status == 'success'){
                setTimeout(function(){
                    window.location.href = 'index.php?r=index';
                }, 800)
            }
        })
    });
JS;
$this->registerJs($script);
?>
<?php $this->endPage() ?>
