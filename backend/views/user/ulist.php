<?php
/**
 * Created by MichealZ.
 * Description: 用户列表模版
 * Date: 2015/8/19
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-medium-2-3 uk-container-center">
        <table class="uk-table uk-table-hover uk-table-striped">
            <caption>客服列表</caption>
            <thead>
            <tr>
                <th>姓名</th>
                <th>角色</th>
                <th>电子邮件地址</th>
                <th>权限等级</th>
                <th>删除用户</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        德玛西亚
                    </td>
                    <td>
                        <div class="uk-badge">管理员</div>
                    </td>
                    <td>123@qq.com</td>
                    <td>
                        <i class="uk-icon-minus-square-o"><img src="<?= IMG_URL ?>uk-icon-minus-square-o.png"/></i>
                        <div class="uk-badge">9</div>
                        <i class="uk-icon-plus-square-o"><img src="<?= IMG_URL ?>uk-icon-plus-square-o.png"/></i>
                    </td>
                    <td>
                        <div class="uk-badge uk-badge-danger">不可删除</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        啦啦啦
                    </td>
                    <td>
                        <div class="uk-badge uk-badge-warning">普通用户</div>
                    </td>
                    <td>456@qq.com</td>
                    <td>
                        <a href=""><i class="uk-icon-minus-square"><img src="<?= IMG_URL ?>uk-icon-minus-square.png"/></i></a>
                        <div class="uk-badge">9</div>
                        <a href=""><i class="uk-icon-plus-square"><img src="<?= IMG_URL ?>uk-icon-plus-square.png"/></i></a>
                        <a href=""><div class="uk-badge uk-badge-success">设为管理员</div></a>
                    </td>
                    <td>
                        <a id="trash" href="">
                            <i class="uk-icon-hover uk-icon-medium uk-icon-trash-o"><img src="<?= IMG_URL ?>uk-icon-trash-o.png"/></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <article class="uk-article uk-width-medium-2-3 uk-container-center uk-margin-large-top">
        <ul class="uk-list uk-list-striped">
            <li>权限等级分为 1-9 9个等级，1为最低权限，9为最高。</li>
            <li>新注册用户权限为 1 。</li>
            <li> 9 是管理员权限，该权限可以操作所有用户账户，但不能操作管理员账户。虽然管理员可以将别人提升为管理员。这主要是为了避免恶意操作。</li>
            <li><b>请谨慎使用删除操作。</b></li>
        </ul>
    </article>
</div>
<?php
$script = <<< JS
    $('#trash').mouseover(function(){
        $(this).find('img').attr('src', '../web/img/uk-icon-trash-o-hover.png');
    }).mouseout(function(){
        $(this).find('img').attr('src', '../web/img/uk-icon-trash-o.png');
    });
JS;
$this->registerJS($script);
?>