<?php
/**
 * Created by MichealZ.
 * Description: 用户登录模版
 * Date: 2015/8/19
 */
$cookie = Yii::$app->request->cookies;
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <div style="display:none;" id="notice" class="uk-alert uk-alert-danger">
                <ul>
                    <li></li>
                </ul>
            </div>
            <div class="uk-form uk-container-center">
                <fieldset data-uk-margin>
                    <legend>请登录</legend>
                    <input type="hidden" name="_token" value="">
                    <div class="uk-form-row uk-form-icon">
                        <i class="uk-icon-user"><img src="<?= IMG_URL ?>uk-icon-envelope.png"></i>
                        <input id="email" type="email" class="form-control" placeholder="请输入登录邮箱"<?php if($cookie->has('remember')) echo 'value="' . $cookie->get('email')->value .  '"';?>>
                    </div>
                    <br>
                    <div class="uk-form-password uk-form-row uk-form-icon">
                        <i class="uk-icon-lock"><img src="<?= IMG_URL ?>uk-icon-lock.png"></i>
                        <input id="password" type="password" placeholder="请输入密码">
                    </div>
                    <div class="uk-form-row">
                        <input id="remember" type="checkbox" value="1" <?php if($cookie->has('remember')){echo 'checked=checked';}?>> 记住此账号
                    </div>
                    <div class="uk-form-row">
                        <button id="regist" type="submit" class="uk-button uk-button-primary">提交</button>
                        <a class="uk-button uk-button-link" href="index.php?r=user/enteracc">忘记密码</a>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
    $('#regist').click(function(){
        var flag = '',
            email = $('#email').val(),
            password = $('#password').val(),
            remember = $('input[type=checkbox]:checked').val();
        if(!email.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))
            flag = '请正确填写您的邮箱!';
        else if(password.length < 6 || password.length > 12)
            flag = '密码长度必须在6-12位之间!';
        if(flag != ''){
            $('#notice').css('display', 'block').find('li').text(flag);
            return false;
        }
        var json = {
            'email': email,
            'password': password,
            'remember': remember
        };
        $.post('index.php?r=user/loginact', json, function(data){
            var json = JSON.parse(data);
            if(json.status == 'success')
            {
                alert(json.msg);
                window.location.href = 'index.php?r=index';
            }
            else
               $('#notice').css('display', 'block').find('li').text(json.msg);
        });
    });
JS;
$this->registerJs($script);
?>