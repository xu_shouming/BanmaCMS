<?php
/**
 * Created by MichealZ.
 * Description: 忘记密码 邮箱填写页
 * Date: 2015/8/26
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <div style="display:none;" id="notice" class="uk-alert uk-alert-danger">
                <ul>
                    <li></li>
                </ul>
            </div>
            <div class="uk-form uk-container-center">
                <fieldset data-uk-margin>
                    <legend>重置密码</legend>
                    <input type="hidden" class="uk-width-1-1" name="_token" value="">
                    <div class="uk-form-row">
                        <input id="email" type="email" class="form-control" name="email" placeholder="请输入登录邮箱" value="">
                    </div>
                    <br>
                    <div class="uk-form-row">
                        <button id="send" type="submit" class="uk-button uk-button-primary">发送重置链接</button>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
    $('#send').click(function(){
        var email = $('#email').val();
        if(!email.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)){
            $('#notice').css('display', 'block').find('li').text('请正确填写您的邮箱!');
            return false;
        }
        var json = {'email': email};
        /*校验用户邮箱是否填写正确,邮箱验证成功后请求发送邮件方法*/
        $.post('index.php?r=user/validemail', json, function(data){
            var tmp = JSON.parse(data);
            if(tmp.status == 'success'){ //邮箱验证成功
                $.post('index.php?r=user/sendmail', json, function(data){
                    var tmp = JSON.parse(data);
                    if(tmp.status == 'success')
                        window.location.href = 'index.php?r=user/notice';
                    else
                        window.location.href = 'index.php?r=user/notice&error=1';
                });
            }
            else
               $('#notice').css('display', 'block').find('li').text(tmp.msg);
        });
    });
JS;
$this->registerJs($script);
?>