<?php
/**
 * Created by MichealZ.
 * Description: 忘记密码 邮箱填写页
 * Date: 2015/8/26
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <div style="display:none;" id="notice" class="uk-alert uk-alert-danger">
                <ul>
                    <li></li>
                </ul>
            </div>
            <div class="uk-form uk-container-center">
                <fieldset data-uk-margin>
                    <img style="float:left;" src="<?= IMG_URL . $msg['img']?>">
                    <h2 style="float:left; margin-left: 10px; margin-top: 10px; font-size: 27px;"><?= $msg['content']?></h2>
                </fieldset>
            </div>
        </div>
    </div>
</div>