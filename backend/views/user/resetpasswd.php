<?php
/**
 * Created by MichealZ.
 * Description: 用户登录模版
 * Date: 2015/8/19
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <div style="display:none;" id="notice" class="uk-alert uk-alert-danger">
                <ul>
                    <li></li>
                </ul>
            </div>
            <div class="uk-form uk-container-center">
                <fieldset data-uk-margin>
                    <legend>重置密码</legend>
                    <input type="hidden" name="_token" value="">
                    <div class="uk-form-row uk-form-icon">
                        <i class="uk-icon-user"><img src="<?= IMG_URL ?>uk-icon-lock.png"></i>
                        <input id="password" type="password" class="form-control" placeholder="请输入新密码">
                    </div>
                    <br>
                    <div class="uk-form-password uk-form-row uk-form-icon">
                        <i class="uk-icon-lock"><img src="<?= IMG_URL ?>uk-icon-lock.png"></i>
                        <input id="repassword" type="password" placeholder="请重复新密码">
                    </div>
                    <div class="uk-form-row">
                        <button id="resetPass" type="submit" class="uk-button uk-button-primary">提交</button>
                    </div>
                    <input type="hidden" id="email" value="<?= $email ?>">
                </fieldset>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
    $('#resetPass').click(function(){
        var email = $('#email').val(),
            password = $('#password').val(),
            repassword = $('#repassword').val();
        if(password.length < 6 || password.length > 12){
            $('#notice').css('display', 'block').find('li').text('密码长度必须在6-12位之间!');
            return false;
        }
        else if(password != repassword){
            $('#notice').css('display', 'block').find('li').text('两次输入的密码不一致!');
            return false;
        }
        var json = {'email': email, 'password': password};
        $.post('index.php?r=user/resetpasswdact', json, function(data){
            var json = JSON.parse(data);
            if(json.status == 'success')
                window.location.href = 'index.php?r=user/notice&resetsuccess=1';
            else
               $('#notice').css('display', 'block').find('li').text(json.msg);
        });
    });
JS;
$this->registerJs($script);
?>