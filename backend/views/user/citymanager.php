<?php
/**
 * Created by MichealZ.
 * Description: 城市管理员列表模版
 * Date: 2015/8/19
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <a href="index.php?r=user/citymanageradd" class="uk-button uk-button-primary"><i class="uk-icon uk-icon-plus"><img src="<?= IMG_URL ?>uk-icon-plus.png"/></i> 新增城市管理员</a>
            <table class="uk-table uk-table-hover uk-table-striped">
                <caption>城市管理员列表</caption>
                <thead>
                <tr>
                    <th>编号</th>
                    <th>电子邮件</th>
                    <th>姓名</th>
                    <th>城市</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>123@qq.com</td>
                    <td>德玛西亚</td>
                    <td>艾欧尼亚</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>