<?php
/**
 * Created by MichealZ.
 * Description: 城市管理员添加模版
 * Date: 2015/8/19
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <form class="uk-form uk-container-center uk-form-horizontal" role="form" method="POST" action="{{ url('admin/city-admin') }}">
                <fieldset data-uk-margin>
                    <legend>添加城市管理员</legend>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="uk-form-row">
                        <label  class="uk-form-label">邮箱</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>
                    <div class="uk-form-row">
                        <label  class="uk-form-label">姓名</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="uk-form-row">
                        <label  class="uk-form-label">密码</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <div class="uk-form-row">
                        <label  class="uk-form-label">城市</label>
                        <div class="uk-autocomplete uk-form" data-uk-autocomplete="{source:'/admin/statistic/city-auto-complete', minLength: 2, delay: 100}">
                            <input type="text" class="form-control" name="city" required>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="uk-form-row">
                        <button type="submit" class="uk-button uk-button-primary">提交</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
