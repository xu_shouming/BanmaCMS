<?php
/**
 * Created by MichealZ.
 * Description: 用户登录模版
 * Date: 2015/8/20
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-medium-1-3 uk-container-center">
        <div class="uk-panel">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox uk-slidenav-position" style="display:none;margin-left:auto;margin-right:auto;width:200px;height:50px;top:151px;">
                <a class="uk-modal-close uk-close uk-close-alt" href="#"></a>
                <div class="uk-lightbox-content"></div>
                <div class="uk-modal-spinner"></div>
            </div>
            <div style="display:none;" id="notice" class="uk-alert uk-alert-danger">
                <ul>
                    <li></li>
                </ul>
            </div>
            <div class="uk-form uk-container-center">
                <fieldset data-uk-margin>
                    <legend>请注册</legend>
                    <div class="uk-form-row uk-form-icon">
                        <i class="uk-icon-user"><img src="<?= IMG_URL ?>uk-icon-user.png"></i>
                        <input type="text" class="form-control" id="username" placeholder="请输入用户名" value="">
                    </div>
                    <br>
                    <div class="uk-form-row uk-form-icon">
                        <i class="uk-icon-envelope"><img src="<?= IMG_URL ?>uk-icon-envelope.png"></i>
                        <input type="email" class="form-control" id="email" placeholder="请输入登录邮箱" value="">
                    </div>
                    <br>
                    <div class="uk-form-password uk-form-row uk-form-icon">
                        <i class="uk-icon-lock"><img src="<?= IMG_URL ?>uk-icon-lock.png"></i>
                        <input type="text" class="form-control" id="password" placeholder="请输入密码" onfocus="this.type='password'">
                    </div>
                    <br>
                    <div class="uk-form-password uk-form-row uk-form-icon">
                        <i class="uk-icon-lock"><img src="<?= IMG_URL ?>uk-icon-lock.png"></i>
                        <input type="text" class="form-control" id="repassword" placeholder="请再次输入密码" onfocus="this.type='password'">
                    </div>
                    <br>
                    <div class="uk-form-password uk-form-row uk-form-icon">
                        <i class="uk-icon-lock"><img src="<?= IMG_URL ?>vcode.png"></i>
                        <input id="vcode" type="text" style="width:126px;" placeholder="请输入验证码">
                        <img id="change" src="index.php?r=user/vcode" />
                    </div>
                    <div class="uk-form-row">
                        <button id="regist" type="submit" class="uk-button uk-button-primary">提交</button>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
    /*验证码点击更换*/
    $('#change').click(function(){
        $('#change').attr('src', 'index.php?r=user/vcode&_s' + Math.random());
    });
    /*注册*/
    $('#regist').click(function(){
        var flag = '',
            username = $('#username').val(),
            email = $('#email').val(),
            password = $('#password').val(),
            repassword = $('#repassword').val(),
            vcode = $('#vcode').val();
        if(username == '')
            flag = '请填写您的用户名!';
        else if(!email.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))
            flag = '邮箱格式不正确!';
        else if(password.length < 6 || password.length > 12)
            flag = '密码长度必须在6-12位之间!';
        else if(password != repassword)
            flag = '两次输入的密码不一致!';
        else if(!vcode)
            flag = '验证码不正确!';
        if(flag != ''){
            $('#notice').css('display', 'block').find('li').text(flag);
            return false;
        }
        var json = {
            'username': username,
            'email': email,
            'password': password,
            'vcode': vcode
        };
        $.post('./index.php?r=user/registeract', json, function(data){
            var json = JSON.parse(data);
            if(json.status == 'success')
            {
                alert(json.msg);
                window.location.href = 'index.php?r=index';
            }
            else
               $('#notice').css('display', 'block').find('li').text(json.msg);
        });
    });
JS;
$this->registerJs($script);
?>