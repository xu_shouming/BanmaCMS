<?php
/**
 * Created by MichealZ.
 * Description: 司机管理列表模版
 * Date: 2015/8/19
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-3-3 uk-container-center">
        <div class="uk-panel">
                <div class="uk-text-large uk-text-primary">

                </div>
                <div class="uk-article-divider"></div>
            <a href="index.php?r=driver/operation&act=add" class="uk-button uk-button-primary"><i class="uk-icon uk-icon-plus"><img src="<?= IMG_URL ?>uk-icon-plus.png"/></i> 新增项目</a>
            <div style="display: inline-block;" id="search">
                <form class="uk-search" data-uk-search action="">
                    <img style="margin-left: 14px; margin-right: -29px;" src="<?= IMG_URL ?>uk-search.png"/>
                    <input class="uk-search-field uk-form-width-large" type="search" name="key" placeholder="在此输入搜索...">
                </form>
            </div>
            <a href="" class="uk-button uk-button-primary uk-float-right"><i class="uk-icon uk-icon-file-excel-o"><img src="<?= IMG_URL ?>uk-icon-file-excel-o.png"/></i> 导出Excel(全部司机信息)</a>
            <table class="uk-table uk-table-hover uk-table-striped">
                <caption>已注册司机列表</caption>
                <thead>
                <tr>
                    <th>编号</th>
                    <th>姓名</th>
                    <th>身份证号</th>
                    <th>推荐人</th>
                    <th>车牌号</th>
                    <th>电话</th>
                    <th>审核照片</th>
                    <th>车辆类型</th>
                    <th>注册时间</th>
                    <th>当月在线</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>10</td>
                        <td>德玛西亚</td>
                        <td>555</td>
                        <td>1</td>
                        <td>2</td>
                        <td>134444444</td>
                        <td>
                            <a class="uk-button uk-text-danger" href="" data-uk-lightbox="{group:'group-{{$item->id}}'}"  data-lightbox-type="image" title="身份证-正面">身份证-正面</a>
                            <a class="uk-button uk-text-danger" href="" data-uk-lightbox="{group:'group-{{$item->id}}'}"  data-lightbox-type="image" title="身份证-背面">身份证-背面</a>
                            <a class="uk-button uk-text-danger" href="" data-uk-lightbox="{group:'group-{{$item->id}}'}"  data-lightbox-type="image" title="驾驶证">驾驶证</a>
                            <a class="uk-button uk-text-danger" href="" data-uk-lightbox="{group:'group-{{$item->id}}'}"  data-lightbox-type="image" title="车辆行驶证">车辆行驶证</a>
                        </td>
                        <td>
                            <div class="uk-badge uk-badge-notification" style="background-color: #8a2be2">微面斑马</div>
                        </td>
                        <td>2015-05-05</td>
                        <td>
                            <div class="uk-badge" style="background-color: #6495ed;">12 小时</div>
                        </td>
                        <td>
                            <div class="uk-badge uk-badge-success">已审核</div>
                        </td>
                        <td>
                            <div style="display: inline-block; position: relative;" data-uk-dropdown="{mode:'click'}">
                                <button class="uk-button">操作
                                <i class="uk-icon-caret-down-s"><img src="<?= IMG_URL ?>uk-icon-caret-down-s.png"/></i>
                                </button>
                                <div class="uk-dropdown">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li><a class="uk-text-" href="index.php?r=driver/operation&act=edit">编辑资料</a></li>
                                        <li><a class="uk-text-" href="">重置密码</a></li>
                                        <li class="uk-nav-divider"></li>
                                        <li>
                                            <a href="">
                                                <div class="uk-text-success">通过审核</div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>