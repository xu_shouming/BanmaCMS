<?php
/**
 * Created by MichealZ.
 * Description: 编辑司机模版
 * Date: 2015/8/19
 */
?>
<link href="<?= CSS_URL ?>form-advanced.almost-flat.min.css" rel="stylesheet" type="text/css" />
<link href="<?= CSS_URL ?>form-select.almost-flat.min.css" rel="stylesheet" type="text/css" />
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <form class="uk-form uk-container-center uk-form-horizontal" role="form" method="POST" action="">
                <input name="_method" type="hidden" value="PUT">
                <fieldset data-uk-margin>
                    <legend>基本信息</legend>
                    <div class="uk-form-row">
                        <label  class="uk-form-label">姓名</label>
                        <input type="text" class="form-control" name="name" value="" required>
                    </div>
                    <div class="uk-form-row">
                        <label  class="uk-form-label">电话号码</label>
                        <input type="text" class="form-control" name="phone" value="" required readonly>
                    </div>
                    <div class="uk-form-row">
                        <label  class="uk-form-label">身份证号</label>
                        <input type="text" class="form-control" name="identify_card_number" value="" required readonly>
                    </div>
                </fieldset>
                <fieldset data-uk-margin>
                    <legend>车辆信息</legend>
                    <div class="uk-form-row">
                        <label class="uk-form-label">车牌号</label>
                        <input type="text" class="form-control" name="car_num" required value="">
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">车型</label>
                        <select name="type">
                            <option value="1">微面斑马</option>
                            <option value="2">中面斑马</option>
                            <option value="3">货车斑马</option>
                            <option value="4">冷链斑马</option>
                            <option value="5">两轮斑马</option>
                        </select>
                    </div>

                </fieldset>
                <fieldset>
                    <legend>附加信息</legend>
                    <div class="uk-form-row">
                        <label class="uk-form-label">车型描述</label>
                        <input type="text" class="form-control" name="car_type_description" value="">
                    </div>
                    <br>
                    <div class="uk-form-row">
                        <button type="submit" class="uk-button uk-button-primary">提交</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= JS_URL ?>form-select.min.js"></script>