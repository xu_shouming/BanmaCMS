<?php
/**
 * Created by MichealZ.
 * Description: 司机状态统计模版
 * Date: 2015/8/20
 */
?>
<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-small-1-3 uk-container-center">
        <div class="uk-panel">
            <table class="uk-table uk-table-hover uk-table-striped">
                <caption>统计数据</caption>
                <tbody>
                <tr>
                    <td>今日注册司机</td>
                    <td>
                        <div class="uk-badge uk-badge-notification">
                            200
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>