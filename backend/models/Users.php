<?php
/**
 * Created by MichealZ.
 * Description: 用户登录/注销/添加/修改/删除
 * Date: 2015/8/19
 */
namespace backend\models;
use Yii;
    class Users extends \yii\db\ActiveRecord
    {
        /**
         * Created by MichealZ.
         * Description: 用户注册
         * Date: 2015/8/25
         * param: 要插入的数据
         * return: string or bool
         */
        public static function userReg($data)
        {
            $obj = new self();
            $obj->username = $data['username'];
            $obj->password = $data['password'];
            $obj->email = $data['email'];
            $obj->created_at = $data['created_at'];
            $obj->role_id = $data['role_id'];
            if($obj->save())
                return Yii::$app->db->getLastInsertID();
            else
                return false;
        }

        /**
         * Created by MichealZ.
         * Description: 用户注册验证邮箱是否存在
         * Date: 2015/8/25
         * param: 注册邮箱
         * return: object or bool
         */
        public static function userVemail($email)
        {
            return self::find()->select(['id'])->where(['email' => $email])->one();
        }

        /**
         * Created by MichealZ.
         * Description: 用户登录验证邮箱是否存在
         * Date: 2015/8/25
         * param: 登录邮箱
         * return: object or bool
         */
        public static function loginValidate($email)
        {
            return self::find()->where($email)->one();
        }

        /**
         * Created by MichealZ.
         * Description: 密码重置操作
         * Date: 2015/8/27
         * param: 修改者邮箱
         * return: string or empty
         */
        public static function resetPasswd($data)
        {
            $id = self::find()->select(['id'])->where(['email' => $data['email']])->one();
            $obj = self::findOne($id->id);
            $obj->password = $data['password'];
            $obj->updated_at = $data['updated_at'];
            return $obj->save();
        }
    }
